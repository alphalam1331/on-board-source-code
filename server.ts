import path from 'path'
import { print } from 'listening-on'
import { port } from './config'
import { spaceRoutes } from './space-routes'
import { userRoutes } from './user-routes'
import { projectRoutes } from './project-routes'
import { sessionRoutes } from './session'
import { app, httpServer } from './http'
import express from 'express'
import { setIO, socketRoutes } from './socket'
import { loggedInOnly } from './guard'


setIO()

httpServer.listen(port, () => {
	print(port)
})

app.use(express.urlencoded({ extended: false }))
app.use(express.json())

app.use(sessionRoutes)
app.use(socketRoutes)
app.use(express.static('public'))
app.use(spaceRoutes)
app.use(userRoutes)
app.use(projectRoutes)
//dont place any route below this!!!
app.use(loggedInOnly, express.static('space'))

app.use((req, res) => {
	res.status(404).sendFile(path.resolve(path.join('public', '404.html')))
})