import express from 'express'
import { Server as HTTPServer } from 'http'


export const app = express()
export const httpServer = new HTTPServer(app)

