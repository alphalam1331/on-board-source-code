const projectsList = document.querySelector('#projects-bar')
const projectTemplate = document.querySelector('#project-template')
const publicList = document.querySelector('#public-channel')
const privateList = document.querySelector('.channels-list')
const channelTemplate = document.querySelector('#channel-template')
const messageForm = document.querySelector('#message-form')
const messageContainer = document.querySelector('#messages-container')
const messageTemplate = document.querySelector('#message-template')
const memberList = document.querySelector('#member-list')
const memberTemplate = document.querySelector('#member-template')
const createChannelForm = document.querySelector('#create-channel-form')

function toggleSignup() {
    document.querySelector("#popup-signup").classList.toggle("hidden")
}

//connect to socketIO
const socket = io();

//pin the scrollbar of the chatroom to the bottom
function scrollToBottom() {
    messageContainer.scrollTop = messageContainer.scrollHeight
}

scrollToBottom()

function toggleMemberList() {
    document.querySelector('#member-list-container').classList.toggle("hidden")
}

//load the current user and all projects and channels that he/she is in
let currentUser;
loadRoom()

async function loadRoom() {
    let res = await fetch('/user/current')
    currentUser = (await res.json())
    // console.log(currentUser)
    res = await fetch(`/user/load/${currentUser.id}/?projects=true&followings=true/`)
    let userData = await res.json()
    // console.log (userData)
    document.querySelector('.profile-pic').src = `img/propic/${userData.propic}`

    //load projects that is participating
    let projects = userData.projects
    loadProjects(projects)
    // console.log (projects)

    //load projects that is following
    let followings = userData.followings
    loadProjects(followings)
}

function loadProjects(projectIDList) {
    projectIDList.forEach(async (project) => {
        let projectData = (await (await fetch(`/project/load/${project}/?all=true&rooms=true`)).json())
        // console.log(projectData)
        let item = projectTemplate.content.cloneNode(true)
        item.querySelector('.logo').textContent = projectData.title
        item.querySelector('.logo').id = project
        item.querySelector('.logo').onclick = chooseProject
        projectsList.append(item)
    })
}

//change room
let currentRoom = {};

async function chooseProject() {
    cleanMessages()
    //Change room title
    currentRoom.pid = this.id
    let project = this.textContent.trim()
    document.querySelector('#current-project').textContent = project;
    document.querySelector('#current-channel').textContent = '';

    let pid = this.id;
    //load members info
    memberList.innerHTML = '' //first clean off the member-list for later appending
    let members = await (await fetch(`/project/load/${pid}/members`)).json()
    for (let member of members) {
        // console.log({ member})
        let item = memberTemplate.content.cloneNode(true)
        item.querySelector('.user-name').textContent = member.username
        item.querySelector('.profile-pic').src = `/img/propic/${member.propic}`
        memberList.append(item)
    }

    //load channels 
    let rooms = (await (await fetch(`/project/load/${pid}/?rooms=true`)).json()).rooms
    let followings = (await (await fetch(`/user/load/${currentUser.id}/?followings=true`)).json()).followings

    publicList.innerHTML = ''
    privateList.innerHTML = ''

    //attach create channel function
    if (followings.indexOf(+pid) == -1) {
        document.querySelector('#create-channel-button').classList.remove('hidden')
    }
    //append channels to channel list
    rooms.forEach(room => {
        let item = channelTemplate.content.cloneNode(true)
        item.querySelector('.channel').textContent = room.name
        item.querySelector('.channel').id = `${pid}:${room.id}`
        item.querySelector('.channel').onclick = chooseChannel

        if (room.accessibility == 'public') {
            publicList.append(item)
        } else {
            if (followings.indexOf(+pid) == -1) {
                privateList.append(item)
            } else {
                privateList.innerHTML = '--Members Only--'
                document.querySelector('#create-channel-button').classList.add('hidden')
                return
            }
        }
    })
}

function chooseChannel() {
    let channel = this.textContent.trim()
    document.querySelector('#current-channel').textContent = channel
    currentRoom.roomName = document.querySelector('#current-room').textContent.trim()
    splitChannelID = this.id.split(':')
    currentRoom.pid = splitChannelID[0]
    currentRoom.roomID = splitChannelID[1]

    cleanMessages()
    loadMessages(currentRoom.pid, currentRoom.roomID)
    // console.log('User: ', currentUser)
    // console.log('Room that chosen: ', currentRoom)
    socket.emit('join/room',
        {
            currentUser,
            currentRoom
        })
    //unlock the message form
    document.querySelector('#form-content').removeAttribute('disabled')
    document.querySelector('#form-content').placeholder = "Type message here..."
    document.querySelector('#message-form>label').removeAttribute('hidden')
    document.querySelector('#send').removeAttribute('disabled')
}

function cleanMessages() {
    //clean up the message area
    messageContainer.innerHTML = ''
}

// load messages from database
async function loadMessages(pid, roomID) {
    let messages = await (await fetch(`/project/load/${pid}/${roomID}/messages`)).json()
    if (messages.length != 0) {
        messages.forEach(message => {
            postMessage(message.username, message.content)
        })
    }

}

//create channel form pop up control
function toggleCreateForm() {
    document.querySelector("#popup-container").classList.toggle("hidden")
}

function toggleUserDropdown() {
    const userDropdown = document.querySelector("#user-dropdown")
    if (userDropdown.classList.value.includes("hidden")) {
        // hide other dropdown
        document.querySelectorAll(".dropdown").forEach(elem => { elem.classList.add("hidden") })
        document.querySelector("#user-dropdown").classList.remove("hidden")
    } else {
        document.querySelector("#user-dropdown").classList.add("hidden")
    }
}

window.addEventListener('click', event => {
    if (event.target.matches('#popup-container')) {
        document.querySelector("#popup-container").classList.toggle("hidden")
    }
})

//createChannel function
createChannelForm.addEventListener('submit', async (event) => {
    event.preventDefault()
    let pid = currentRoom.pid
    let name = createChannelForm.channelName.value.trim()
    let res = await fetch(createChannelForm.action, {
        method: createChannelForm.method,
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ pid, name })
    })
    let result = await res.json()
    if (res.status >= 300) {
        document.querySelector('#create-result').textContent = result.message
    } else {
        // console.log(result.room.newRoomID)
        createChannelForm.reset()
        document.querySelector("#popup-container").classList.toggle("hidden")
        //append to channel list if success.
        let item = channelTemplate.content.cloneNode(true)
        item.querySelector('.channel').textContent = name
        item.querySelector('.channel').id = `${pid}:${result.room.newRoomID}`
        item.querySelector('.channel').onclick = chooseChannel
        privateList.append(item)
    }
})

//send message to the server (and to the socket io)
messageForm.addEventListener('submit', async (event) => {
    // console.log('Sending message to the socket: ', currentRoom)
    event.preventDefault();

    let msg = new FormData()

    msg.append('uid', currentUser.id)
    msg.append('userName', currentUser.username)
    msg.append('pid', currentRoom.pid)
    msg.append('roomID', currentRoom.roomID)
    msg.append('roomName', currentRoom.roomName)
    msg.append('content', messageForm.content.value)
    msg.append('file', messageForm.file.files[0])


    res = await fetch('/chat/upload/msg', {
        method: "POST",
        body: msg
    });


    //send data to socket if server successfully insert into table 
    msg = await res.json()
    // console.log('Message that sent to the Socket: ', msg)
    socket.emit('uploadMessage', msg)
    socket.emit('newMessage', currentRoom.roomID, currentRoom.pid, currentUser.id, messageForm.content.value)

    messageForm.reset()
})

//receive socket message from server socket
socket.on('postMessage', (data) => {
    let sender = data.userName;
    let content = data.content;
    // console.log('data that received from Socket: ', sender, content)

    //manipulate the DOM
    postMessage(sender, content)
})

socket.on('greeting', (msg) => {
    console.log(msg)
})

function postMessage(sender, content) {
    let item = messageTemplate.content.cloneNode(true);
    item.querySelector('.sender').textContent = sender;
    item.querySelector('.content').textContent = content;

    let messageDiv = item.querySelector('.message')
    //check if the sender = currentUser
    if (sender == currentUser.username) {
        messageDiv.classList.add('ego')
    } else {
        messageDiv.classList.add('tu')
    }

    messageContainer.append(item)
    scrollToBottom()
}