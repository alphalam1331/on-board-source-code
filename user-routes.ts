import express from 'express';
import multer from 'multer'
import { client } from './db'
import bcryptjs from 'bcryptjs'
import './session'
import { accountOwnerOnly } from './guard';


export const upload = multer({ dest: './public/img/propic' })
export let userRoutes = express.Router()

userRoutes.post('/user/login', async (req, res) => {

    // missing field(s)
    if (!req.body.username) {
        res.status(400).json({ message: "missing username" })
        return
    }
    if (!req.body.password) {
        res.status(400).json({ message: "missing password" })
        return
    }

    // no missing data >> query database
    try {
        let data = await client.query(`SELECT id, password FROM users WHERE username=$1`, [req.body.username])

        // wrong username
        if (data.rows.length == 0) {
            res.status(401).json({ message: "username is not registered" })
            return
        }

        // correct username >> check password
        let hash = data.rows[0].password
        let match = await bcryptjs.compare(req.body.password, hash)
        if (match) {
            req.session.user = {
                id: data.rows[0].id,
                username: req.body.username
            }
            res.status(200).json({ message: "successfully logged in", user: req.body.username })
        } else {
            res.status(401).json({ message: "wrong password" })
        }
    } catch (err) {
        console.log(err)
        res.status(500).json({ message: "database error" })
    }

})

userRoutes.post('/user/logout', async (req, res) => {
    req.session.user = undefined
    res.status(200).json({ message: "logged out" })
})

userRoutes.post('/user/signup', upload.single('propic'),async (req, res) => {
    try {
        let propic = req.file?.filename
        let newInUser = req.body
        let usernameResult = await (await client.query(`select username from users where username = $1`, [newInUser.username])).rowCount;
        let emailResult = await (await client.query(`select email from users where email = $1`, [newInUser.email])).rowCount;
        if (usernameResult != 0) {
            res.status(400).json({ message: `${newInUser.username} already exists.` })
            return
        }
        if (emailResult != 0) {
            res.status(400).json({ message: `${newInUser.email} is used.` })
            return
        }
        newInUser.password = await bcryptjs.hash(newInUser.password, 10)
        newInUser.propic = propic
        await client.query(`insert into users (username, password, email, propic) values ($1, $2, $3, $4)`, [newInUser.username, newInUser.password, newInUser.email, newInUser.propic])
        res.status(200).json({ message: `Successfully register!` })
    } catch (error) {
        console.log(error)
    }
})

// get current user from session
// TODO: change error response
userRoutes.get('/user/current', (req, res) => {
    if (!req.session.user) {
        res.json({})
    } else {
        res.json(req.session.user)
    }
})


// load data of specific user
userRoutes.get('/user/load/:uid', async (req, res, next) => {
    let id = +req.params.uid

    try {
        let result = await client.query(`SELECT username, email, propic, occupation, bio FROM users WHERE id=$1`, [id])
        if (result.rows.length == 0) {
            res.status(404).json({ message: "user not found" })
            return
        }

        let data = result.rows[0]

        // load optional data
        if (req.query.projects || req.query.all) {
            result = await client.query(`
        SELECT projects.id, projects.title, join_project.role
        FROM join_project
        JOIN projects
        ON projects.id=join_project.project_id
        WHERE join_project.user_id=$1
        `, [id])
            data.projects = result.rows
        }
        if (req.query.interests || req.query.all) {
            result = await client.query(`SELECT name FROM categories WHERE id IN 
        (SELECT category_id FROM interest_category WHERE user_id=$1)`, [id])
            data.interests = result.rows.map(row => row.name)
        }
        if (req.query.skills || req.query.all) {
            result = await client.query(`SELECT name FROM skills WHERE id IN 
        (SELECT skill_id FROM have_skill WHERE user_id=$1)`, [id])
            data.skills = result.rows.map(row => row.name)
        }
        if (req.query.projects || req.query.all) {
            result = await client.query(`SELECT project_id FROM join_project 
            WHERE user_id=$1 AND role IN ('crew', 'captain')`, [id])
            data.projects = result.rows.map(row => row.project_id)
        }
        if (req.query.followings) {
            result = await client.query(`SELECT project_id FROM join_project 
            WHERE user_id=$1 AND role IN ('follower', 'pending')`, [id])
            data.followings = result.rows.map(row => row.project_id)
        }
        res.status(200).json(data)
    } catch (err) {
        console.log(err)
        res.status(500).json({ message: err })
    }
})


userRoutes.put('/user/edit/:uid', accountOwnerOnly, async (req, res, next) => {
    // TODO: add guard function
    let uid = req.params.uid

    try {
        // update db
        console.log("in /user/edit")
        if (req.body.username) {
            await client.query(`UPDATE users SET username=$1 WHERE id=$2`, [req.body.username, uid])
        }
        if (req.body.bio) {
            await client.query(`UPDATE users SET bio=$1 WHERE id=$2`, [req.body.bio, uid])
        }
        if (req.body.email) {
            await client.query(`UPDATE users SET email=$1 WHERE id=$2`, [req.body.email, uid])
        }
        if (req.body.occupation) {
            await client.query(`UPDATE users SET occupation=$1 WHERE id=$2`, [req.body.occupation, uid])
        }
        if (req.body.interests) {
            let interests: string[] = req.body.interests.split(",")
            interests = interests.map(item => item.trim().toLowerCase())
            await client.query(`DELETE FROM interest_category WHERE user_id=$1`, [uid])
            for (let interest of interests) {
                let data = await client.query(`SELECT * FROM categories WHERE name=$1`, [interest])
                let categoryId: number
                if (data.rowCount == 0) {
                    await client.query(`INSERT INTO categories (name) VALUES($1)`, [interest])
                    categoryId = (await client.query(`SELECT id FROM categories WHERE name=$1`, [interest])).rows[0].id
                } else {
                    categoryId = data.rows[0].id
                }
                await client.query(`INSERT INTO interest_category (user_id, category_id) VALUES($1, $2)`, [uid, categoryId])
            }
        }
        if (req.body.skills) {
            let skills: string[] = req.body.skills.split(",")
            skills = skills.map(item => item.trim().toLowerCase())
            await client.query(`DELETE FROM have_skill WHERE user_id=$1`, [uid])
            for (let skill of skills) {
                let data = await client.query(`SELECT * FROM skills WHERE name=$1`, [skill])
                let skillId: number
                if (data.rowCount == 0) {
                    await client.query(`INSERT INTO skills (name) VALUES($1)`, [skill])
                    skillId = (await client.query(`SELECT id FROM skills WHERE name=$1`, [skill])).rows[0].id
                } else {
                    skillId = data.rows[0].id
                }
                await client.query(`INSERT INTO have_skill (user_id, skill_id) VALUES($1, $2)`, [uid, skillId])
            }
        }

        res.status(200).json({ message: "successfully updated user info" })
    } catch (err) {
        console.log(err)
        res.status(500).json({ message: "Database Error" })
    }
})