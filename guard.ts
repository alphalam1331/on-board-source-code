import {Request, Response, NextFunction} from 'express'
import { client } from './db'

// check if user is logged in
export function loggedInOnly (req: Request, res: Response, next: NextFunction) {
    if (req.session.user) {
        next()
    } else {
        res.status(401).json({message: "You're not logged in"})
    }
}

// check if current user is the owner of current user profile
export async function accountOwnerOnly (req: Request, res: Response, next: NextFunction) {
    let uid = req.params.uid? req.params.uid : req.body.uid
    if (!uid) {
        res.status(400).json({message: "missing user id"})
        return
    } 
    let currentUid = req.session.user?.id
    if (uid != currentUid) {
        res.status(401).json({message: "This is not your profile!"})
        return
    } else {
        next()
    }
}

// check if current user is the captain of current project page
export async function captainOnly (req: Request, res: Response, next: NextFunction) {
    let pid = req.body.pid? req.body.pid : req.params.pid
    if (!pid) {
        res.status(400).json({message: "missing project id"})
        return
    } 
    let currentUid = req.session.user?.id
    let result = await client.query(`SELECT from join_project WHERE user_id=$1 AND project_id=$2 AND role='captain'`, [currentUid, pid])
    if (result.rowCount == 0) {
        res.status(401).json({message: "You're not the captain"})
        return
    } else {
        next()
    }
}

