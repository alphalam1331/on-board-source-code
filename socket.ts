import { Server as socketServer } from 'socket.io'
import { httpServer } from './http'
import express, { Request, NextFunction } from 'express'
import { sessionMiddleware } from './session'
import { client } from './db'
import { loggedInOnly } from './guard'


export const io = new socketServer(httpServer)
export const socketRoutes = express.Router()

let currentSockets = {}
/* format: {
    [uid]: [SocketId],
    [uid]: [SocketId],
    etc.
} */

type Message = {
    userName: string;
    content: string;
}

export function setIO() {
    io.use((socket, next) => {
        let req = socket.request as Request
        let res = req.res!
        sessionMiddleware(req, res, next as NextFunction)
    })

    io.on('connection', (socket) => {
        let req = socket.request as Request
        if (req.session.user) {
            currentSockets[req.session.user.id] = socket.id
        }
        console.log('current sockets: ', currentSockets)
        console.log('socket connected: ', socket.id)

        socket.on('uploadMessage', (data) => {
            // console.log('SocketIO: message received from client: ', data)
            let msg: Message = {
                userName: data.userName,
                content: data.content
            }
            io.to(data.roomID).emit('postMessage', msg)
        })

        socket.on('join/room', (data) => {
            // console.log (data)
            let currentUser = data.currentUser
            let currentRoom = data.currentRoom
            const rooms = socket.rooms
            // console.log ('Before leaving room', rooms)

            rooms.forEach((room) => {
                if (socket.id != room) {
                    socket.leave(room)
                }
            })
            socket.join(currentRoom.roomID)
            // console.log ('After leaving and joining room', rooms)
            socket.broadcast.emit('greeting', `${currentUser.username} has just entered this room.`)
            socket.to(currentRoom.roomID).emit('greeting', `Welcome! You are now in Room: ${currentRoom.roomName}`)
        })

        // notification ( type: join | abandon | pending | reject | task )
        socket.on('notification', async (type, info) => {

            // define receivers
            let data = await client.query( /* SQL */
                `SELECT user_id FROM join_project WHERE project_id=$1`, [info.pid])
            let receivers = data.rows.map(row => row.user_id)
            if (type == "abandon") {
                receivers.push(req.session.user?.id)
            } else if (type == "reject") {
                receivers.push(info.uid)
            }
            console.log("receivers: ", receivers)

            // display info
            let displayInfo = {
                uid: info.uid,
                pid: info.pid,
                user: info.uid? (await client.query(`SELECT username FROM users WHERE id=$1`, [info.uid])).rows[0].username : undefined,
                project: info.pid? (await client.query(`SELECT title FROM projects WHERE id=$1`, [info.pid])).rows[0].title : undefined,
                remarks: info.remarks
            }

            // action to receiver
            for (let uid of receivers) {
                // send event
                if (currentSockets[uid]) {
                    io.to(currentSockets[uid]).emit('notification', type, displayInfo)
                }
                // update db
                let infoStr = JSON.stringify(info)
                let count = await client.query( /* SQL */
                    `SELECT id FROM notifications WHERE user_id=$1 ORDER BY created`, [uid])
                if (count.rowCount >= 7) {
                    await client.query( /* SQL */
                        `DELETE from notifications WHERE id=$1`, [count.rows.shift().id])
                }
                await client.query( /* SQL */
                    `INSERT INTO notifications (user_id, genre, info) VALUES ($1, $2, $3)`, [uid, type, infoStr])
            }

        })

        // new message notification
        socket.on('newMessage', async (rid, pid, uid, msg) => { // rid: room id
            console.log("pid: ", pid)
            let accessibility = (await client.query( /* SQL */
                `SELECT accessibility FROM rooms WHERE id=$1`, [rid])).rows[0].accessibility

            // define receivers
            let receivers = []
            let data = await client.query( /* SQL */
                `SELECT user_id, role FROM join_project WHERE project_id=$1`, [pid])
            if (accessibility == "public") { // public channels
                receivers = data.rows.map(row => row.user_id)
            } else if (accessibility == "private") { // private channels
                receivers = data.rows.filter(row => row.role == "captain" || row.role == "crew")
                    .map(row => row.user_id)
            }
            console.log("new msg receivers: ", receivers)

            // display info (to be sent to frontend)
            let displayInfo = {
                uid: uid,
                pid: pid,
                rid: rid,
                user: (await client.query(`SELECT username FROM users WHERE id=$1`, [uid])).rows[0].username,
                project: (await client.query(`SELECT title FROM projects WHERE id=$1`, [pid])).rows[0].title,
                room: (await client.query(`SELECT name FROM rooms WHERE id=$1`, [rid])).rows[0].name,
                msg: msg
            }

            // send event
            for (let uid of receivers) {
                if (currentSockets[uid]) {
                    io.to(currentSockets[uid]).emit('newMessage', displayInfo)
                }
            }
        })

        // disconnect
        socket.on('disconnect', () => {
            if (req.session.user) {
                delete currentSockets[req.session.user.id]
                console.log('current sockets: ', currentSockets)
            }
        })
    })
}


socketRoutes.get('/socket/load/notification', loggedInOnly, async (req, res) => {
    let uid = req.session.user!.id
    let data = await client.query( /* SQL */
        `SELECT genre, info FROM notifications WHERE user_id=${uid} ORDER BY id DESC`)
    let result = []
    for (let row of data.rows) {
        let type = row.genre
        let info = JSON.parse(row.info)
        info.user = info.uid? (await client.query(`SELECT username FROM users WHERE id=$1`, [info.uid])).rows[0].username : undefined
        info.project = info.pid? (await client.query(`SELECT title FROM projects WHERE id=$1`, [info.pid])).rows[0].title : undefined
        result.push({ type: type, info: info })
    }
    res.json(result)
})

socketRoutes.get('/socket/load/message', loggedInOnly, async (req, res) => {
    let currentUid = req.session.user?.id
    let data = await client.query( /* SQL */ 
        `SELECT messages.id, messages.room_id, messages.sender_id, messages.content, rooms.name, rooms.project_id, users.username, projects.title
        FROM messages
        JOIN rooms ON messages.room_id=rooms.id
        JOIN users ON messages.sender_id=users.id
        JOIN projects ON rooms.project_id=projects.id
        WHERE (((rooms.project_id IN (SELECT project_id FROM join_project
									 WHERE user_id=$1)
            AND rooms.accessibility='public')
            OR (rooms.project_id IN (SELECT project_id FROM join_project 
									 WHERE user_id=$1 AND role IN ('captain', 'crew'))
            AND rooms.accessibility='private')))
            AND messages.sender_id <> $1
		ORDER BY messages.id DESC
		LIMIT 7`, [currentUid])
            
    let result = []
    for (let row of data.rows) {
        result.push({
            msg: row.content,
            pid: row.project_id,
            project: row.title,
            rid: row.room_id,
            room: row.name,
            uid: row.sender_id,
            user: row.username
        })
    }
    res.json(result)
})