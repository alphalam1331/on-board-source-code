drop database onboard;
create database onboard;
\c onboard;

create table users(
    id serial primary key
    , username varchar(25) unique not null
    , password varchar (64) not null
    , email varchar (50) unique not null
    , created timestamp default current_timestamp 
    , bio text
    , occupation varchar(50)
    , propic varchar(255)
);


create table notifications (
    id serial primary key
    , user_id integer references users (id)
    , genre varchar(25)
    , info varchar(100)
    , created timestamp default current_timestamp
);

create table categories (
    id serial primary key
    , name varchar(255) not null
);


create table interest_category (
    user_id integer    
    , foreign key (user_id) references users (id)
    , category_id integer
    , foreign key (category_id) references categories (id)
);


create table skills (
    id serial primary key
    , name varchar(255) not null
    , description text DEFAULT 'This is a skillful skill.'
);

create table have_skill (
    user_id INTEGER
    , foreign key (user_id) references users (id)
    , skill_id INTEGER
    , foreign key (skill_id) references skills (id)
);

insert into users (username, password, email, propic, bio) 
values
    ('jack_sparrow', '$2a$10$YLVwL/6DBAsN.D4yBU37BOc23/x/WxeD9s1Wlkloa8MmpkfoHXmtG', 'blackjack@hotmail.com', 'sample01.jpg'
    ,'Jack Sparrow was a legendary pirate of the Seven Seas, and the irreverent trickster of the Caribbean. A captain of equally dubious morality and sobriety, a master of self-promotion and self-interest, Jack fought a constant and losing battle with his own best tendencies. Jack''s first love was the sea, his second, his beloved ship the Black Pearl.')
    ,('se7ensea', '$2a$10$pGmvpwE4jubaAdsZbnNEiOowOI5pYbciQDZcIf0QDJSYtF1oQp3JC', 'kingofsevenseas@gmail.com', 'sample02.jpg'
    ,'I chased a man across the seven seas. The pursuit cost me my crew, my commission, and my life. The Seven Seas is a phrase commonly used in reference to seven of the major oceans of the world. The phrase could also be used to indicate the entirety of the world itself.')
    ,('blackbeard', '$2a$10$Q.Hz/SM1VYOTMumNKTNQy..Gn4B8T/q00idQdxauXxc3LPKjtHk8G', 'bbbeard@gmail.com', 'sample03.jpg'
    ,'Marshall D. Teach, most commonly referred to by his epithet Blackbeard, is the captain-turned-admiral of the Blackbeard Pirates, the captain of their tenth ship, and currently one of the Four Emperors. He is also the only known person in history to wield the powers of two Devil Fruits. He started out his pirate career as a member of the Whitebeard Pirates'' 2nd division until he murdered Thatch, the 4th division commander, for the Yami Yami no Mi and defected.')
;

insert into categories (name)
VALUES
    ('Business')
    ,('Technology')
    ,('Art')
    ,('Environment')
    ;

insert into skills (name, description)
VALUES
    ('navigation', 'good at using compass')
    ,('cooking', 'able to cook 572 cuisines')
    ,('robbery', 'You can''t catch me.')
    ;

insert into interest_category (user_id, category_id)
values
((select id from users where username = 'jack_sparrow'), (select id from categories where name = 'Business'))
, ((select id from users where username = 'jack_sparrow'), (select id from categories where name = 'Art'))
, ((select id from users where username = 'se7ensea'), (select id from categories where name = 'Business'))
, ((select id from users where username = 'blackbeard'), (select id from categories where name = 'Technology'))
;

insert into have_skill (user_id, skill_id)
VALUES
((select id from users where username = 'jack_sparrow'), (select id from skills where name = 'cooking'))
, ((select id from users where username = 'jack_sparrow'), (select id from skills where name = 'navigation'))
, ((select id from users where username = 'se7ensea'), (select id from skills where name = 'cooking'))
, ((select id from users where username = 'se7ensea'), (select id from skills where name = 'robbery'))
, ((select id from users where username = 'blackbeard'), (select id from skills where name = 'robbery'))
, ((select id from users where username = 'blackbeard'), (select id from skills where name = 'navigation'))
;

create table projects(
    id serial primary key,
    title varchar(100) not null,
    description text,
    created timestamp default current_timestamp,
    start_date date default current_date,
    end_date date
);

create table project_category(
    project_id integer references projects (id),
    category_id integer references categories (id)
);

create table join_project(
    id serial primary key,
    user_id integer references users (id),
    project_id integer references projects (id),
    role varchar(25)
);

create table images(
    id serial primary key,
    filename varchar(255) unique not null,
    project_id integer references projects (id)
);


insert into projects (title, description)
values
    ('Indie Film about a Nerd', 'This is an indie film project about the geeky life of a programmer. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'),
    ('Plant Trees to Save our Planet', 'We need more trees. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'),
    ('NFT Idea that Will Blow Your Mind', 'NFT. NFT. NFT. NFT. NFT. NFT. NFT. NFT. NFT. NFT. NFT. NFT. NFT. NFT. NFT. NFT. NFT. NFT. NFT. NFT.')
;

insert into project_category (project_id, category_id)
values
((select id from projects where title='Indie Film about a Nerd'), (select id from categories where name='Art')),
((select id from projects where title='Indie Film about a Nerd'), (select id from categories where name='Technology')),
((select id from projects where title='Plant Trees to Save our Planet (Testing)'), (select id from categories where name='Environment')),
((select id from projects where title='NFT Idea that Will Blow Your Mind'), (select id from categories where name='Art')),
((select id from projects where title='NFT Idea that Will Blow Your Mind'), (select id from categories where name='Technology')),
((select id from projects where title='NFT Idea that Will Blow Your Mind'), (select id from categories where name='Business'));

insert into images (filename, project_id)
values
('project01-1.jpg',(select id from projects where title='Indie Film about a Nerd')),
('project01-2.jpg',(select id from projects where title='Indie Film about a Nerd')),
('project01-3.jpg',(select id from projects where title='Indie Film about a Nerd')),
('project02.jpg',(select id from projects where title='Plant Trees to Save our Planet')),
('project03.jpg',(select id from projects where title='NFT Idea that Will Blow Your Mind'));

insert into join_project (user_id, project_id, role)
values
((select id from users where username = 'jack_sparrow'), (select id from projects where title='Indie Film about a Nerd'), 'crew'),
((select id from users where username = 'blackbeard'), (select id from projects where title='Indie Film about a Nerd'), 'captain'),
((select id from users where username = 'se7ensea'), (select id from projects where title='Plant Trees to Save our Planet'), 'captain'),
((select id from users where username = 'jack_sparrow'), (select id from projects where title='NFT Idea that Will Blow Your Mind'), 'captain'),
((select id from users where username = 'blackbeard'), (select id from projects where title='NFT Idea that Will Blow Your Mind'), 'crew'),
((select id from users where username = 'se7ensea'), (select id from projects where title='NFT Idea that Will Blow Your Mind'), 'crew');

-- add tasks, join_task, required_skill tables

create table tasks(
    id serial primary key
    , project_id integer not NULL
    , title varchar(100) not NULL
    , description text
    , created timestamp default current_timestamp
    , deadline date
);

insert into tasks (project_id, title, description, deadline) 
values 
((select id from projects where id = '1'), 'casting', 'target: 1000 kelefes', '2021-12-31')
, ((select id from projects where id = '2'), 'buy seed', 'planting tree in a seedless way?', '2022-2-28') 
, ((select id from projects where id = '3'), 'marketing research', 'Is NFT a scam?', '2022-9-9') 
, ((select id from projects where id = '1'), 'find lunch supplier', 'WE ARE! FREE LUNCH SOCEITY!', '2022-3-17') 
, ((select id from projects where id = '2'), 'obtain a degree', 'how can we save the Earth without related knowledge?', '2022-5-7') 
, ((select id from projects where id = '1'), 'R sponsor', 'everyone but Maxim', '2021-12-28') 
, ((select id from projects where id = '3'), 'compose a piece of 595 notes', 'We are the next Sakamoto Ryūichi.', '2022-1-15') 
;

create table join_task (
    user_id integer references users (id)
    , join_project_id integer references join_project (id) ON DELETE CASCADE
    , task_id integer references tasks (id) ON DELETE CASCADE
);


insert into join_task (user_id, join_project_id, task_id)
values
((select user_id from join_project where id = 1),(select id from join_project where id = 1), (select id from tasks where title = 'casting'))
,((select user_id from join_project where id = 2),(select id from join_project where id = 2), (select id from tasks where title = 'find lunch supplier'))
,((select user_id from join_project where id = 2),(select id from join_project where id = 2), (select id from tasks where title = 'R sponsor'))
,((select user_id from join_project where id = 3),(select id from join_project where id = 3), (select id from tasks where title = 'buy seed'))
,((select user_id from join_project where id = 3),(select id from join_project where id = 3), (select id from tasks where title = 'obtain a degree'))
,((select user_id from join_project where id = 5),(select id from join_project where id = 5), (select id from tasks where title = 'marketing research'))
,((select user_id from join_project where id = 6),(select id from join_project where id = 6), (select id from tasks where title = 'compose a piece of 595 notes'))
;

create table required_skill (
task_id integer references tasks (id) ON DELETE CASCADE,
skill_id integer references skills (id)
);

insert into required_skill (task_id, skill_id)
VALUES
((select id from tasks where title = 'casting' ),(select id from skills where name = 'navigation' ))
, ((select id from tasks where title = 'buy seed' ),(select id from skills where name = 'navigation' ))
, ((select id from tasks where title = 'buy seed' ),(select id from skills where name = 'cooking' ))
, ((select id from tasks where title = 'marketing research' ),(select id from skills where name = 'navigation' ))
, ((select id from tasks where title = 'find lunch supplier' ),(select id from skills where name = 'cooking'))
, ((select id from tasks where title = 'find lunch supplier' ),(select id from skills where name = 'robbery' ))
, ((select id from tasks where title = 'obtain a degree' ),(select id from skills where name = 'robbery' ))
, ((select id from tasks where title = 'R sponsor' ),(select id from skills where name = 'navigation' ))
, ((select id from tasks where title = 'R sponsor' ),(select id from skills where name = 'robbery' ))
, ((select id from tasks where title = 'compose a piece of 595 notes' ),(select id from skills where name = 'robbery'))
;

create table rooms (
    id serial primary key
    , project_id integer references projects (id)
    , name varchar(255) not null
    , accessibility varchar(25) default 'private'
);

insert into rooms (project_id, name, accessibility)
VALUES
((select id from projects where id = 1), 'Public', 'public')
, ((select id from projects where id = 1), 'Hollywood', 'private')
, ((select id from projects where id = 2), 'Public', 'public')
, ((select id from projects where id = 2), 'Aquarium', 'private')
, ((select id from projects where id = 3), 'Public', 'public')
;

create table messages(
    id serial primary key
    , project_id integer references projects (id)
    , room_id integer references rooms (id)
    , sender_id integer references users (id)
    , content TEXT
    , image varchar(255)
    , created timestamp default current_timestamp
    , reply_to integer references messages (id)
);
