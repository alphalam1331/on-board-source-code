import express from 'express';
import session from 'express-session'
import dotenv from 'dotenv'

dotenv.config()
export let sessionRoutes = express.Router()

declare module 'express-session' {
    interface SessionData {
        user: {
            id: number,
            username: string,
        }
    }
}

if (!process.env.SESSION_SECRET) {
	throw new Error('missing SESSION_SECRET in env')
}

export const sessionMiddleware = session({
	secret: process.env.SESSION_SECRET,
	resave: true,
	saveUninitialized: true,
})

sessionRoutes.use(sessionMiddleware)