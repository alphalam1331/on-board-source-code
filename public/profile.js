let urlParams = new URLSearchParams(location.search)
let currentProfileUid = urlParams.get("uid")
let categories = {
    interests: [],
    skills: []
}


async function loadProfile() {
    // get user data from server
    let data = await fetch(`/user/load/${currentProfileUid}?all=true`)
    data = await data.json()
    let currentUser = await (await fetch('/user/current')).json()
    if (currentProfileUid == currentUser.id) {
        document.body.classList.remove("guest")
    }

    // select fields from html
    let bio = document.querySelector("#profile-bio")
    let username = document.querySelector("#profile-username")
    let occupation = document.querySelector("#profile-occupation")
    let email = document.querySelector("#profile-email")
    let interests = document.querySelector("#profile-interests")
    let skills = document.querySelector("#profile-skills")
    let propic = document.querySelector("#profile-propic")
    let cardContainer = document.querySelector("#projects-profile")

    // insert basic data
    bio.textContent = data.bio ? data.bio : "Dead men tell no tales, but I'm sure you've a lot to tell!"
    propic.src = data.propic ? `/img/propic/${data.propic}` : "/img/propic/default.jpg"
    username.textContent = data.username
    email.textContent = data.email
    occupation.textContent = data.occupation ? data.occupation : "Pirate, we assume"

    // insert interests & skills 
    loadCategories("interests", data.interests)
    loadCategories("skills", data.skills)
    // if (data.interests.length > 0) {
    //     interests.innerHTML = ""
    //     for (let item of data.interests) {
    //         let tag = document.createElement("a")
    //         tag.className = "tag"
    //         tag.textContent = item.substring(0,1).toUpperCase() + item.substring(1).toLowerCase()
    //         tag.href = `/search.html?categories=${item}`
    //         interests.append(tag)
    //         categories.interests.push(item)
    //     }
    // } else {
    //     interests.innerHTML = "Where's your compass pointing to, matey?"
    // }

    // if (data.skills.length > 0) {
    //     skills.innerHTML = ""
    //     for (let item of data.skills) {
    //         let tag = document.createElement("a")
    //         tag.className = "tag"
    //         tag.textContent = item.substring(0,1).toUpperCase() + item.substring(1).toLowerCase()
    //         tag.href = `/search.html?skills=${item}`
    //         skills.append(tag)
    //         categories.skills.push(item)
    //     }
    // } else {
    //     skills.innerHTML = "Robbery or something?"
    // }

    // insert project cards
    for (let pid of data.projects) {
        await generateCard(cardContainer, pid, 4)
    }

}
loadProfile()

function loadCategories(property, arr) {
    let div = document.querySelector(`#profile-${property}`)
    categories[property] = []

    if (arr.length > 0) {
        div.innerHTML = ""
        for (let item of arr) {
            let tag = document.createElement("a")
            tag.className = "tag"
            tag.textContent = item.substring(0, 1).toUpperCase() + item.substring(1).toLowerCase()
            tag.href = `/search.html?${(property == "interests") ? "categories" : "skills"}=${item}`
            div.append(tag)
            categories[property].push(item)
        }
    } else {
        div.innerHTML = (property == "interests") ? "Where's your compass pointing to, matey?" : "Robbery or something?"
    }
}




// Edit Profile
let popupEdit = document.createElement("div")
popupEdit.className = "popup-container hidden"
popupEdit.id = "popup-edit"
popupEdit.innerHTML = /* HTML */
    `<div class="popup">
        <h2>Edit</h2>
        <form action="" id="edit-form">
            <label>My <span id="edit-property">property</span>:
            <div id="edit-input-div"></div></label><br>
            <div id="edit-status"></div>
            <input type="submit" value="Save">
        </form>
    </div>`
document.querySelector("#popup-div").append(popupEdit)


let editBtns = document.querySelectorAll(".edit-btn")
for (let btn of editBtns) {
    btn.addEventListener('click', () => {
        const property = btn.id.substring(5)
        let oldVal = document.querySelector(`#profile-${property}`).textContent
        document.querySelector("#edit-property").textContent = property

        if (property == "bio") {
            document.querySelector("#edit-input-div").innerHTML = /* HTML */
                `<textarea id="edit-input" name="new-value" rows="8" cols="40"></textarea>`
        } else {
            document.querySelector("#edit-input-div").innerHTML = /* HTML */
                `<input id="edit-input" name="new-value" type="text" value="">`
            if (property == "interests" || property == "skills") {
                oldVal = categories[property].join(",")
            }
        }

        document.querySelector("#edit-input").value = oldVal
        document.querySelector("#popup-edit").classList.remove("hidden")
    })
}

let editForm = document.querySelector("#edit-form")
editForm.addEventListener("submit", async (event) => {
    event.preventDefault()
    let uid = currentProfileUid
    let reqBody = {}
    let property = editForm.querySelector("#edit-property").textContent
    let newVal = editForm.querySelector("#edit-input").value
    let message = document.querySelector("#edit-status")
    reqBody[property] = newVal

    try {
        let res = await fetch(`/user/edit/${uid}`, {
            method: "PUT",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(reqBody)
        })

        if (res.status > 200) {
            message.textContent = (await res.json()).message
            return
        }
    } catch (err) {
        console.log(err)
        message.textContent = "Server Error"
        return
    }

    if (property == "interests" || property == "skills") {
        console.log("loading categories")
        let arr = newVal.split(",").map(item => item.trim())
        loadCategories(property, arr)
    } else {
        document.querySelector(`#profile-${property}`).textContent = newVal
    }
    document.querySelector("#popup-edit").classList.add("hidden")
})

