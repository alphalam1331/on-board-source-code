// attach navbar
async function attachNavbar() {
    if (document.querySelector("nav")) {
        // check login status
        let isLoggedIn = false
        let res = await (await fetch('/user/current')).json()
        if (res.id) {
            isLoggedIn = true
        }

        let htmlStr = /* HTML */
            `<div class="row p-3 align-items-end justify-content-between">
                <div class="col-auto">
                    <a href="index.html" class="logo px-3">onBoard</a>
                    <div class="search">
                        <form action="" id="nav-search-form">
                            <input type="text" id="input-nav-search">
                            <input type="submit" value="Search">
                        </form>
                    </div>
                </div>
                <div class="col-auto mb-1 mt-2 ${isLoggedIn ? "hidden" : ""}" id="nav-icons-guest">
                    <a href="#"><span onclick="toggleSignup()">Sign Up</span></a>
                    <button class="ml-2" onclick="toggleLogin()">Login</button>
                </div>

                <div class="col-auto mb-1 mt-2 ${isLoggedIn ? "" : "hidden"}" id="nav-icons">
                    <i class="mx-2 fas fa-plus" onclick="toggleCreateProject()"></i>
                    <i class="mx-2 fas fa-feather-alt message-btn" onclick="toggleMsgDropdown()"></i>
                    <i class="mx-2 fas fa-ship notification-btn" onclick="toggleNotifDropdown()"></i>
                    <i class="ml-2 fas fa-user" onclick="toggleUserDropdown()"></i>
                    <div id="user-dropdown" class="dropdown hidden">
                        <a href="profile.html?uid=${res.id}">Profile</a><br>
                        <a href="#" onclick="logOut()">Logout</a>
                    </div>
                    <div id="notif-dropdown" class="dropdown hidden">
                        <h3 style="text-align: center" class="py-2">Notifications</h3>
                    </div>
                    <div id="msg-dropdown" class="dropdown hidden">
                        <h3 style="text-align: center" class="py-2">New Messages</h3>
                    </div>
                </div>
            </div>`

        document.querySelector("nav").innerHTML = htmlStr

        // search form
        const searchForm = document.querySelector("nav #nav-search-form")
        searchForm.addEventListener('submit', async (event) => {
            event.preventDefault()
            let searchPhrase = searchForm.querySelector("#input-nav-search").value.trim().split(/\s+/).join(",")
            window.location.replace(`/search.html?keywords=${searchPhrase}`)
        })
    }
}
attachNavbar()

// change user icon to propic if logged in
async function attachNavPropic() {
    let res = await fetch('/user/current')
    currentUser = await res.json()

    if (currentUser.id) {
        let userData = await fetch(`/user/load/${currentUser.id}`)
        userData = await userData.json()

        document.querySelector("#nav-icons .fa-user").remove()
        let propic = document.createElement("img")
        propic.src = `/img/propic/${userData.propic}`
        propic.className = "mx-2 propic"
        propic.onclick = toggleUserDropdown
        document.querySelector("#nav-icons").append(propic)
    }
}
attachNavPropic()

// attach login/signup/create boxes
if (document.querySelector("#popup-div")) {
    document.querySelector("#popup-div").innerHTML = /* HTML */
        `<div class="popup-container hidden" id="popup-login">
        <div class="popup">
            <h2>Login</h2>
            <form id="login-form" action="/user/login" method="POST">
                <input id="login-username" name="username" type="text" placeholder="Email/username"> <br>
                <input id="login-password" name="password" type="password" placeholder="Password"> <br>
                <div id="login-status"></div>
                <input type="submit">
            </form>
            <a href="">Forget password?</a>
            <hr> Not yet on board? <a href="">Register now</a>
        </div>
    </div>
    <div class="popup-container hidden" id="popup-signup">
        <div class="popup">
            <h2>Sign Up</h2>
            <form id="signup-form" action="/user/signup" method="POST">
                <input id="signup-username" name="username" type="text" placeholder="Username"> <br>
                <input id="signup-password" name="password" type="password" placeholder="Password"> <br>
                <input id="signup-email" name="email" type="email" placeholder="E-mail"> <br>
                <label for="signup-propic">Upload Profile Picture</label>
                <input id="signup-propic" name="propic" type="file" hidden> <br>
                <input type="submit" value="Submit">
            </form>
            <div id="signup-status"></div>
            <hr> Already on board? <a href="">Login</a>
        </div>
    </div>

    <div class="popup-container hidden" id="popup-create">
        <div class="popup">
            <h2>Create Project</h2>
            <form id="create-project-form" action="/project/create" method="POST" enctype="multipart/form-data">
                <input id="project-title" name="title" type="text" placeholder="Project Title" required> <br>
                <input id="project-captain" name="captain" type ="text" readonly><br>
                <textarea id="project-description" name="description" type="text" placeholder="Describe your project here..." required style="height: 12rem; resize: none" cols="40"></textarea> <br>
                <label for="images">Project Images: </label>
                <input id="images" type="file" name="images" multiple required><br>
                <input id="project-start" name="start" type="text" onfocus="(this.type='date')" onblur="(this.type='text')" placeholder="Start Date" required> <br>
                <input id="project-end" name="end" type="text" onfocus="(this.type='date')" onblur="(this.type='text')" placeholder="End Date" required> <br>
                <input type="submit" value="Submit">
            </form>
            <div id="create-result"></div>
        </div>
    </div>`
    fetch('/user/current').then(res => {
        res.json().then(res => {
            document.querySelector('#project-captain').value = "Captain: " + res.username
        })
    })
}

// attach space button
async function attachSpaceButton() {
    let isLoggedIn = false
    let res = await (await fetch('/user/current')).json()
    if (res.id) {
        isLoggedIn = true
        if (document.querySelector("#space-button-container")) {
            document.querySelector("#space-button-container").innerHTML = /* HTML */
                `<div style="position: relative;">
                    <a id="space-button" href="/space.html">
                        <i class="fab fa-empire"></i>
                    </a>
                </div>`
        }
    }
}

attachSpaceButton()


//////////////////////// Cards //////////////////////////

// generate dummy cards (to be removed when function is complete)
let dummyContainers = document.querySelectorAll(".dummy-card-container")
for (let elem of dummyContainers) {
    for (let i = 0; i < 6; i++) {
        generateCard(elem, i % 3 + 1)
    }
}


// generate dynamic cards
// note: container in html must have class "row card-container"
// note: for narrow containers, pass in bootstrap col width as 3rd arg (optional)
async function generateCard(container, id, size) {
    let data = await fetch(`/project/load/${id}?images=true`)
    data = await data.json()
    let currentUser = await fetch(`/user/current`)
    currentUser = await currentUser.json()

    let card = document.createElement("div")
    card.id = id
    card.className = `col-lg-${size ? size : 3} col-md-4 col-sm-6 col-12 pr-0 pb-3`
    card.innerHTML = /*html*/
        `<div class="card project-card p-2">
            <a href="project.html?pid=${id}"><img src="img/img01.jpg" alt="project image"></a>
            <div class="row my-3 justify-content-start">
                <div class="col-auto">
                    <a href=""><img src="img/propic/default.jpg" alt="" class="propic"></a>
                </div>
                <div class="col p-0">
                    <div class="username">@username</div>
                    <div class="caption-light">Student, Programmer</div>
                </div>
            </div>
            <div class="title">This is the Title of this Interesting Project</div>
            <hr>
            <div class="row justify-content-between">
                <div class="col-auto">
                    <i class="mx-2 mb-2 fas fa-user-ninja"></i>
                    Crew: <span class="crew-count"></span>
                </div>
                <div class="col-auto">
                    <span class="card-status"></span>
                    <a><i class="mx-2 mb-2 fas fa-hiking join-btn"></i></a>
                    <a><i class="mx-2 mb-2 fas fa-binoculars follow-btn"></i></a>
                </div>
            </div>
        </div>`

    // project image
    let projectImg = card.querySelector("img")
    if (data.images.length > 0) {
        projectImg.src = `/img/project/${data.images[0]}`
    } else {
        projectImg.src = "/img/project/default.jpg"
    }

    // project title
    card.querySelector(".title").textContent = data.title

    // captain data
    let captain = data.roles.find(member => member.role == "captain")
    card.querySelector(".username").textContent = "@" + captain.username
    card.querySelector(".propic").src = `/img/propic/${captain.propic}`
    card.querySelector(".propic").parentElement.href = `/profile.html?uid=${captain.id}`
    card.querySelector(".caption-light").textContent = captain.occupation ? captain.occupation : "Pirate, we assume"

    // action icons
    card.querySelector(".crew-count").textContent = data.roles.filter(member => (member.role == "captain" || member.role == "crew")).length
    let currentRole = data.roles.find(member => member.id == currentUser.id)
    if (currentRole) {
        currentRole = currentRole.role
    }
    if (currentUser.id) { // logged in
        resetCardIconStatus(card, currentRole, id)
    } else { // not logged in
        let joinBtn = card.querySelector(".join-btn")
        let followBtn = card.querySelector(".follow-btn")
        for (let btn of [joinBtn, followBtn]) {
            btn.addEventListener('click', () => {
                toggleLogin()
            })
        }
    }
    container.append(card);
}

// reset card icon color & interaction
async function resetCardIconStatus(card, role, id) {
    let joinBtn = card.querySelector(".join-btn")
    let followBtn = card.querySelector(".follow-btn")
    let crewCount = card.querySelector(".crew-count")
    let hasText = false
    if (card.id == "btn-div") {
        hasText = true
    }

    // get current user
    let currentUid = (await (await fetch('/user/current')).json()).id

    // remove old event listeners
    joinBtn.replaceWith(joinBtn.cloneNode(true));
    followBtn.replaceWith(followBtn.cloneNode(true));
    joinBtn = card.querySelector(".join-btn")
    followBtn = card.querySelector(".follow-btn")

    // reset color
    joinBtn.classList.remove("active")
    joinBtn.classList.remove("pending")
    followBtn.classList.remove("active")
    if (role) { // has role
        if (role == "captain" || role == "crew") {
            joinBtn.classList.add("active")
            if (hasText) { joinBtn.textContent = "Abandon Ship :(" }
        } else if (role == "pending") {
            joinBtn.classList.add("pending")
            if (hasText) { joinBtn.textContent = "Requested onBoard" }
        }
        followBtn.classList.add("active")
        if (hasText) { followBtn.textContent = "Unfollow" }
    } else {
        if (hasText) {
            joinBtn.textContent = "Join the Crew!"
            followBtn.textContent = "Follow"
        }
    }

    // join button - new event listener
    if (role == "captain") { // captain
        joinBtn.addEventListener('click', (event) => {
            window.alert("You can't abandon ship as a captain!")
        })
    } else if (role == "crew" || role == "pending") {
        // crew | pending
        joinBtn.addEventListener('click', (event) => {
            fetch(`/project/action/${id}/abandon`).then(res => {
                res.json().then(res => {
                    card.querySelector(".card-status").textContent = res.message
                })
            })
            if (role == "crew") {
                card.querySelector(".crew-count").textContent = +card.querySelector(".crew-count").textContent - 1
                socket.emit('notification', 'abandon', { pid: id, uid: currentUid })
            }
            resetCardIconStatus(card, undefined, id)
        })
    } else { // not crew
        joinBtn.addEventListener('click', (event) => {
            fetch(`/project/action/${id}/join`).then(res => {
                res.json().then(res => {
                    card.querySelector(".card-status").textContent = res.message
                    socket.emit('notification', 'pending', { pid: id, uid: currentUid })
                })
            })
            resetCardIconStatus(card, "pending", id)
        })
    }

    // follow button - new event listener
    if (role == "captain" || role == "crew" || role == "pending") { // captain | crew | pending
        followBtn.addEventListener('click', (event) => {
            window.alert("You can't unfollow while you're onboard!")
        })
    } else if (role == "follower") { // follower 
        followBtn.addEventListener('click', (event) => {
            fetch(`/project/action/${id}/unfollow`).then(res => {
                res.json().then(res => {
                    card.querySelector(".card-status").textContent = res.message
                })
            })
            resetCardIconStatus(card, undefined, id)
        })
    } else { // non-follower
        followBtn.addEventListener('click', (event) => {
            fetch(`/project/action/${id}/follow`).then(res => {
                res.json().then(res => {
                    card.querySelector(".card-status").textContent = res.message
                })
            })
            resetCardIconStatus(card, "follower", id)
        })
    }
}

// display all projects on front page
async function insertAllProjects(container) {
    let data = await fetch('/project/search?all=true')
    let allPid = await data.json()
    for (let id of allPid) {
        await generateCard(container, id)
    }
}

let allProjectContainer = document.querySelector("#projects-all")
if (allProjectContainer) {
    insertAllProjects(allProjectContainer)
}


// notification box
let notifBox = document.createElement("div")
notifBox.className = "card"
notifBox.id = "notif-box"
notifBox.innerHTML = /* HTML */
    `<h3><i class="fas fa-exclamation-circle"></i>  <span id="notif-heading">Notification</span></h3>
    <span id="notif-text">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</span>`
document.body.append(notifBox)
let notifText = notifBox.querySelector("#notif-text")

// function to display notif box
function showNotif(notifHTML, msg) {
    notifBox.querySelector("#notif-heading").textContent = "Notification"
    notifText.innerHTML = notifHTML
    if (msg) {
        notifBox.querySelector(".msg").textContent = `"${msg}"`
        notifBox.querySelector("#notif-heading").textContent = "New Message"
    }
    notifBox.classList.add("show")
    setTimeout(() => {
        notifBox.classList.remove("show")
    }, 5000)
}