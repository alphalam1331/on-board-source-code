
const title = document.querySelector('#proj-title')
const description = document.querySelector('#proj-description')
const imageList = document.querySelector('#proj-image')
const imageTemplate = document.querySelector('#project-image-template')
const memberList = document.querySelector('#member-list')
const pendingList = document.querySelector('#pending-list')
const memberTemplate = document.querySelector('#member-template')
const categoryList = document.querySelector('#proj-categories')
const categoryTemplate = document.querySelector('#category-template')
const skillList = document.querySelector('#proj-skills')
const skillTemplate = document.querySelector('#skill-template')
const taskTemplate = document.querySelector('#task-template')



chooseCard()

function chooseCard() {
    let params = location.search
    let pid = new URLSearchParams(params).get('pid')
    loadProjectData(pid)
}


async function loadProjectData(pid) {
    let res = await fetch(`/project/load/${pid}/?images=true&categories=true&skills=true&tasks=true`)
    let result = await res.json()

    let currentUser = await (await fetch('/user/current')).json()
    let captain = result.roles.find(member => member.role == "captain")
    if (currentUser.id == captain.id) {
        document.body.classList.remove("guest")
    }

    title.textContent = result.title
    description.textContent = result.description
    for (let image of result.images) {
        const item = imageTemplate.content.cloneNode(true)
        item.querySelector('.project-img').src = (`./img/project/${image}`)
        imageList.append(item)
    }

    for (let member of result.roles) {
        const item = memberTemplate.content.cloneNode(true)
        item.querySelector('.username').textContent = member.username
        item.querySelector('.caption-light').textContent = member.role
        item.querySelector('img').src = (`./img/propic/${member.propic}`)
        item.querySelector('img').parentNode.href = `/profile.html?uid=${member.id}`
        if (member.role == "pending") {
            item.querySelector('.pending-btns').classList.remove("hidden")
            item.querySelector('.approve-btn').addEventListener('click', (event) => {
                fetch(`/project/approve/${pid}/${member.id}`).then(() => {
                    socket.emit('notification', "join", { pid: parseInt(pid), uid: member.id })
                })
                // update display
                let thisNode = event.target.parentNode.parentNode
                let clonedNode = thisNode.cloneNode(true)
                thisNode.remove()
                clonedNode.querySelector(".pending-btns").classList.add("hidden")
                clonedNode.querySelector(".caption-light").textContent = "crew"
                document.querySelector('#member-list').append(clonedNode)
            })
            item.querySelector('.reject-btn').addEventListener('click', (event) => {
                fetch(`/project/reject/${pid}/${member.id}`).then(() => {
                    socket.emit('notification', "reject", { pid: parseInt(pid), uid: member.id })
                })
                // update display
                event.target.parentNode.parentNode.remove()
            })
            pendingList.append(item)
        } else if (member.role == "follower") {
        } else {
            memberList.append(item)
        }
    }

    for (let task of result.tasks) {
        console.log(task)
        const item = taskTemplate.content.cloneNode(true)
        item.querySelector('.task-title').textContent = task.title
        let span = document.createElement('span')
        span.innerHTML = ' <i class="far fa-compass"></i>'
        item.querySelector('.task-title').append(span)
        item.querySelector('.task-description').textContent = `Skills needed: ${task.required_skill.join(', ')}`
        if (task.propic) {
            item.querySelector('.propic-list').innerHTML += `<img src=img/propic/${task.propic} alt="" class="propic"></img>`
        }
        document.querySelector('#proj-tasks').append(item)
    }

    for (let category of result.categories) {
        const item = categoryTemplate.content.cloneNode(true)
        item.querySelector('.category').textContent = ` ${category} `
        item.querySelector('.category').href = `/search.html?categories=${category.toLowerCase()}`
        categoryList.append(item)
    }

    for (let skill of result.skills) {
        const item = skillTemplate.content.cloneNode(true)
        item.querySelector('.skill').textContent = ` ${skill} `
        item.querySelector('.skill').href = `/search.html?skills=${skill.toLowerCase()}`
        skillList.append(item)
    }


}

resetActionBtn()
async function resetActionBtn() {
    const div = document.querySelector("#btn-div")
    
    // get current project data
    const params = location.search
    const pid = new URLSearchParams(params).get('pid')
    const projectData = await (await fetch(`/project/load/${pid}`)).json()
    div.querySelector(".crew-count").textContent = projectData.roles.filter(member => (member.role == "captain" || member.role == "crew")).length
    
    // get current user data
    const currentUser = await (await fetch('/user/current')).json()
    let currentUid
    if (currentUser.id) { // if logged in
        currentUid = currentUser.id
    } else { // if not logged in
        let btns = div.querySelectorAll(".action-btn")
        for (let btn of btns) {
            btn.classList.add("hidden")
        }
        return
    }

    // get current user's role
    let currentRole = null
    let memberData = projectData.roles.find(member => member.id == currentUid)
    if (memberData) {
        currentRole = memberData.role
    }

    resetCardIconStatus(div, currentRole, pid, { page: "project" })
}

//pop up edit form
let popupEdit = document.createElement("div")
popupEdit.classList.add("popup-container")
popupEdit.classList.add("hidden")
popupEdit.id = "popup-edit"
popupEdit.innerHTML = /* HTML */
    `<div class="popup">
        <h2>Edit</h2>
        <form action="/project/edit" id="edit-form">
            <label>Project's <span id="edit-property">property</span>:
            <div id="edit-input-div"></div></label><br>
            <div id="edit-status"></div>
            <input type="submit" value="Save">
        </form>
    </div>`
document.querySelector("#popup-div").append(popupEdit)

//add event listener to edit button
let editBtns = document.querySelectorAll(".edit-btn")
for (let btn of editBtns) {
    btn.addEventListener('click', () => {
        const property = btn.id.substring(5)
        const oldVal = document.querySelector(`#proj-${property}`).textContent
        document.querySelector("#edit-property").textContent = property
        document.querySelector("#edit-status").textContent = ''


        //set edit form input type
        if (property == "description") {
            document.querySelector("#edit-input-div").innerHTML = /* HTML */
                `<textarea id="edit-input" name="new-value" rows="8" cols="40" required></textarea>`

        } else if (property == "tasks") {
            document.querySelector("#edit-input-div").innerHTML = /* HTML */
                `<input id="tasks-title" name="title" type="text" placeholder="Task Title" required><br>
            <textarea id="tasks-description" name="description" type="text" placeholder="Describe the task here" required></textarea><br>
            <input id="tasks-skills" name="skills" type="text" placeholder="Skills needed" required><br>
            <input id="start-date" name="start" type="text" onfocus="(this.type='date')" onblur="(this.type='text')" placeholder="Start Date" required><br>
            <input id="end-date" name="end" type="text" onfocus="(this.type='date')" onblur="(this.type='text')" placeholder="End Date" required><br>
            `
        } else {
            document.querySelector("#edit-input-div").innerHTML = /* HTML */
                `<input id="edit-input" name="new-value" type="text" value="" required>`
        }

        //
        if (property == "categories") {
            document.querySelector("#edit-input").value = oldVal.trim().split(/\s+/)
        } else if (property == "tasks") {
            document.querySelector('#tasks-title').value = ''
            document.querySelector('#tasks-description').value = ''
            document.querySelector('#tasks-skills').value = ''
        } else {
            document.querySelector("#edit-input").value = oldVal
        }
        document.querySelector("#popup-edit").classList.remove("hidden")
    })
}

//update database
let editForm = document.querySelector("#edit-form")
editForm.addEventListener("submit", async (event) => {
    event.preventDefault()
    let params = location.search
    let pid = new URLSearchParams(params).get('pid')
    let reqBody = {}
    let property = editForm.querySelector("#edit-property").textContent
    let newVal;
    //retrieve user input
    if (property == "categories") {
        newVal = []
        let cates = editForm.querySelector("#edit-input").value.split(',')
        for (let cate of cates) {
            cate = cate.trim()
            cate = cate[0].toUpperCase() + cate.slice(1).toLowerCase()
            newVal.push(cate)
        }
    } else if (property == "tasks") {
        newVal = {}
        newVal.title = editForm.querySelector("#tasks-title").value
        newVal.description = editForm.querySelector("#tasks-description").value
        newVal.startDate = editForm.querySelector("#start-date").value
        newVal.endDate = editForm.querySelector("#end-date").value
        let skills = editForm.querySelector("#tasks-skills").value.split(',')
        // socket.emit('notification', 'task', {pid: pid})

        newVal.skill = []
        for (let skill of skills) {
            skill = skill.trim()
            skill = skill[0].toUpperCase() + skill.slice(1).toLowerCase()
            // console.log (skill)
            newVal.skill.push(skill)
            // console.log(newVal.skill)
        }
    } else {
        newVal = editForm.querySelector("#edit-input").value
    }

    let message = document.querySelector("#edit-status")
    reqBody['pid'] = pid
    reqBody[property] = newVal

    try {
        let res = await fetch(editForm.action, {
            method: "PUT",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(reqBody)
        })

        let result = await res.json()

        if (property == "categories") {
            // console.log (result)
            document.querySelector(`#proj-${property}`).innerHTML = ''
            for (let category of result.categories) {
                const item = categoryTemplate.content.cloneNode(true)
                item.querySelector('.category').textContent = category
                categoryList.append(item)
            }
        } else if (property == "tasks") {
            // console.log({result})

            const item = taskTemplate.content.cloneNode(true)
            item.querySelector('.task-title').textContent = result.tasks.title
            let description = item.querySelector('.task-description')
            description.textContent = "Skills needed: "

            for (let skill of result.tasks.skill) {
                if (result.tasks.skill.indexOf(skill) > 0) {
                    description.textContent += `, ${skill}`

                } else {
                    description.textContent += skill
                }
            }
            document.querySelector('#proj-tasks').append(item)

        } else {
            // console.log (newVal)
            document.querySelector(`#proj-${property}`).textContent = newVal
        }
        document.querySelector("#popup-edit").classList.add("hidden")
        message.textContent = result.message
        location.reload()
        return
    } catch (err) {
        console.log(err)
        message.textContent = "Server Error"
        return
    }
})



