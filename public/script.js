
const signUpForm = document.querySelector('#signup-form')
const logInForm = document.querySelector('#login-form')
const createForm = document.querySelector('#create-project-form')

// Socket
let socket
async function setupSocket() {
    let res = await (await fetch('/user/current')).json()
    if (res.id) { // if logged in
        socket = io();
        
        socket.on('notification', (type, info) => {
            console.log(type, info)
            showNotif(generateNotifHTML(type, info, res.id))
        })

        socket.on('newMessage', (info) => {
            console.log(info)
            // info.msg = (info.msg.length > 50)? info.msg.substring(0, 47) + "..." : info.msg
            showNotif(generateNotifHTML("message", info, res.id), info.msg)
        })
    }
}
setupSocket()

/////////////////// Notification functions //////////////////////
function generateNotifHTML(type, info, currentUid) {
    const ego = (currentUid == info.uid)
    const userStr = `<a href="/profile.html?uid=${info.uid}">@${info.user}</a>`
    const projectStr = `<a href="/project.html?pid=${info.pid}">${info.project}</a>`

    // New messages
    if (type == "message") {
        if (ego) {return} // no notification for msg sent by the user him/herself
        const roomStr = `<a href="/space.html">/${info.room}</a>`
        notifHTML = /* HTML */ `${userStr}: <span class="msg">(message...)</span><br>
            <span class="caption-light"> (from ${roomStr} of ${projectStr})</span>`
    }

    // Other events
    else if (type == "join") {
        notifHTML = `${ego ? "You are" : userStr + " is"} now onboard! (from: ${projectStr})`
    } else if (type == "abandon") {
        notifHTML = `${ego ? "You" : userStr} just abandoned ship! (from: ${projectStr})`
    } else if (type == "pending") {
        if (ego) {
            notifHTML = `Your onboard request has been sent to the captain of <a href="/project.html?pid=${info.pid}">${info.project}</a>!`
        } else {
            notifHTML = `${userStr} wants to be your crewmate! (from: ${projectStr})`
        }
    } else if (type == "reject") {
        notifHTML = `Captain rejected ${ego? "your" : userStr + "'s"} onboard request :( (from: ${projectStr})`
    } else if (type == "task") {
        notifHTML =  `A new task is created at ${projectStr}!`
    } else {
        notifHTML = "unspecified notification"
    }

    // return innerHTML str
    return notifHTML
}

async function toggleNotifDropdown() {
    const notifDropdown = document.querySelector("#notif-dropdown")
    if (notifDropdown.classList.value.includes("hidden")) {
        // hide other dropdown
        document.querySelectorAll(".dropdown").forEach(elem => { elem.classList.add("hidden") })

        // get notif history
        let data = await (await fetch('/socket/load/notification')).json()
        let currentUid = (await (await fetch('/user/current')).json()).id
        notifDropdown.innerHTML = `<h3 style="text-align: center" class="py-2">Notifications</h3>`
        for (let notif of data) {
            notifDropdown.innerHTML += "<hr>"
            let notifElem = document.createElement("div")
            notifElem.innerHTML = generateNotifHTML(notif.type, notif.info, currentUid)
            notifDropdown.append(notifElem)
        }
        notifDropdown.classList.remove("hidden")
    } else {
        notifDropdown.classList.add("hidden")
    }
}

async function toggleMsgDropdown() {
    const msgDropdown = document.querySelector("#msg-dropdown")
    if (msgDropdown.classList.value.includes("hidden")) {
        // hide other dropdown
        document.querySelectorAll(".dropdown").forEach(elem => { elem.classList.add("hidden") })

        // get msg history
        let data = await (await fetch('/socket/load/message')).json()
        let currentUid = (await (await fetch('/user/current')).json()).id

        // display
        msgDropdown.innerHTML = `<h3 style="text-align: center" class="py-2">New Messages</h3>`
        msgDropdown.classList.remove("hidden")
        for (let msgInfo of data) {
            msgDropdown.innerHTML += "<hr>"
            let notifElem = document.createElement("div")
            notifElem.innerHTML = generateNotifHTML("message", msgInfo, currentUid)
            notifElem.querySelector(".msg").textContent = `"${msgInfo.msg}"`
            msgDropdown.append(notifElem)
        }
    } else {
        msgDropdown.classList.add("hidden")
    }
}

/////////////////////////// Buttons /////////////////////////////
function toggleSpaceButton() {
    const spaceList = document.querySelector("#projects-container").classList
    if (spaceList.value.includes("hidden")) {
        spaceList.remove("hidden")
    } else {
        spaceList.toggle("animate__rotateOutUpRight")
        spaceList.toggle("animate__rotateInDownRight")
    }
}

function toggleUserDropdown() {
    const userDropdown = document.querySelector("#user-dropdown")
    if (userDropdown.classList.value.includes("hidden")) {
        // hide other dropdown
        document.querySelectorAll(".dropdown").forEach(elem => { elem.classList.add("hidden") })
        document.querySelector("#user-dropdown").classList.remove("hidden")
    } else {
        document.querySelector("#user-dropdown").classList.add("hidden")
    }
}

function toggleLogin() {
    document.querySelector("#popup-login").classList.toggle("hidden")
}

function toggleSignup() {
    document.querySelector("#popup-signup").classList.toggle("hidden")
}

function toggleCreateProject() {
    document.querySelector("#popup-create").classList.toggle("hidden")
}

let joinBtns = document.querySelectorAll(".card .fa-hiking") // join project
for (let btn of joinBtns) {
    // TODO:
}


// pop-up event listeners
let popupContainers = document.querySelectorAll(".popup-container")
for (let elem of popupContainers) {
    elem.addEventListener('mousedown', (event) => {
        if (event.target == elem) {
            elem.classList.add("hidden")
        }
    })
}

signUpForm.addEventListener('submit', async (event) => {
    event.preventDefault();
    let user = new FormData()

    user.append('username', signUpForm.username.value)
    user.append('password', signUpForm.password.value)
    user.append('propic', signUpForm.propic.files[0])
    user.append('email', signUpForm.email.value)

    let res = await fetch(signUpForm.action, {
        method: signUpForm.method,
        body: user,
    })
    let result = await res.json()
    if (res.status < 300) {
        //automatically login
        let username = signUpForm.username.value
        let password = signUpForm.password.value

        let res = await fetch(logInForm.action, {
            method: logInForm.method,
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({username, password })
        })
        if (res.status == 200) {
            location.reload()
        } else {
            res = await res.json()
            document.querySelector('#login-status').textContent = res.message
        }
    }
    document.querySelector('#signup-status').textContent = result.message
})


logInForm.addEventListener('submit', async (event) => {
    event.preventDefault()
    let username = logInForm.username.value
    let password = logInForm.password.value

    let res = await fetch(logInForm.action, {
        method: logInForm.method,
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ username, password })
    })
    if (res.status == 200) {
        location.reload()
    } else {
        res = await res.json()
        document.querySelector('#login-status').textContent = res.message
    }
})

//create-project
createForm.addEventListener('submit', async (event) => {
    event.preventDefault()
    //check the current user
    let res = await (await fetch('user/current/')).json()
    let userID = res.id
    let userName = res.username

    const newProject = new FormData()
    newProject.append('captainID', userID)
    newProject.append('captain', userName)
    newProject.append('title', createForm.title.value.trim())
    newProject.append('description', createForm.description.value.trim())
    newProject.append('start', createForm.start.value)
    newProject.append('end', createForm.end.value)
    for (let i = 0; i < createForm.images.files.length; i++) {
        newProject.append('images', createForm.images.files[i])
    }

    res = await fetch(createForm.action, {
        method: createForm.method,
        body: newProject
    })

    let result = await res.json()

    if (result.message === 'Successfully Created!') {
        location.reload()
    } else {
        document.querySelector('#create-result').textContent = result.message
    }
})


async function logOut() {
    await fetch('/user/logout', {
        method: "POST"
    })
    location.reload()
}
