const categoryDiv = document.querySelector("#category-filters")
const skillDiv = document.querySelector("#skill-filters")
const filterForm = document.querySelector("#filter-search-form")
const kwForm = document.querySelector("#keyword-search-form")
const resultDiv = document.querySelector("#search-results")


// load page
async function loadFilters() {
    const categories = await (await fetch('/project/categories')).json()
    const skills = await (await fetch('/project/skills')).json()
    console.log(categories, skills)

    for (let category of categories) {
        let checkbox = document.createElement("div")
        checkbox.innerHTML = /* HTML */
            `<input type="checkbox" id="category-game" name="categories" value="game">
        <label for="game">game</label><br>`
        let input = checkbox.querySelector("input")
        let label = checkbox.querySelector("label")

        input.id = `category-${category.toLowerCase()}`
        input.value = category
        label.for = category
        label.textContent = category.substring(0,1).toUpperCase() + category.substring(1).toLowerCase()
        categoryDiv.append(checkbox)
    }

    for (let skill of skills) {
        let checkbox = document.createElement("div")
        checkbox.innerHTML = /* HTML */
            `<input type="checkbox" id="skill-navigation" name="skills" value="navigation">
        <label for="navigation">navigation</label><br>`
        let input = checkbox.querySelector("input")
        let label = checkbox.querySelector("label")

        input.id = `skill-${skill.toLowerCase()}`
        input.value = skill
        label.for = skill
        label.textContent = skill.substring(0,1).toUpperCase() + skill.substring(1).toLowerCase()
        skillDiv.append(checkbox)
    }

    checkUrlParam()
}
loadFilters()

// check if url contains params
function checkUrlParam() {
    const urlParams = new URLSearchParams(location.search)
    if (urlParams.get("keywords")) {
        let searchPhrase = urlParams.get("keywords").split(",").join(" ")
        generateSearchResults(searchPhrase)
        document.querySelector("#input-keyword").value = searchPhrase
    } else {
        if (urlParams.get("categories")) {
            let categories = urlParams.get("categories").split(",")
            for (let category of categories) {
                document.querySelector(`#category-${category.toLowerCase()}`).checked = true
            }
        }
        if (urlParams.get("skills")) {
            let skills = urlParams.get("skills").split(",")
            for (let skill of skills) {
                console.log("skill: ", skill)
                document.querySelector(`#skill-${skill.toLowerCase()}`).checked = true
            }
        }
        generateFilterResults()
    }
}



// result generators
async function generateSearchResults(searchPhrase) {
    let keywords = searchPhrase.trim().split(/\s+/).join(",")
    const result = await (await fetch(`/project/search?keywords=${keywords}`)).json()

    // display results
    resultDiv.innerHTML = ""
    for (let id of result) {
        generateCard(resultDiv, id, 4)
    }
}

async function generateFilterResults() {
    let checkedBoxes = categoryDiv.querySelectorAll("input:checked")
    let categories = []
    for (let box of checkedBoxes) {
        categories.push(box.value)
    }

    checkedBoxes = skillDiv.querySelectorAll("input:checked")
    let skills = []
    for (let box of checkedBoxes) {
        skills.push(box.value)
    }

    // fetch results
    let url = "/project/search?"
    url += (categories.length > 0) ? `categories=${categories.join(",")}` : ""
    url += (skills.length > 0) ? `&skills=${skills.join(",")}` : ""
    const result = await (await fetch(url)).json()

    // display results
    resultDiv.innerHTML = ""
    for (let id of result) {
        generateCard(resultDiv, id, 4)
    }
}




// AJAX form submission
filterForm.addEventListener('submit', async event => {
    event.preventDefault()
    await generateFilterResults()
})

kwForm.addEventListener('submit', async (event) => {
    event.preventDefault()
    let searchPhrase = kwForm.querySelector("#input-keyword").value
    generateSearchResults(searchPhrase)
})