import express from 'express';
import multer from 'multer'
import { client } from './db';

export const upload = multer({ dest: './space/uploads' })

export let spaceRoutes = express.Router()

spaceRoutes.post('/chat/upload/msg', upload.single('file'), uploadMessage);

async function uploadMessage(req: express.Request, res: express.Response, next: express.NextFunction) {
    //insert msg into table: messages
    try {
        const msg = req.body
        req.file? msg.file = req.file : msg.file = null

        // console.log ('Data received from client: ', msg)

        // let roomData = await (await client.query(`select * from rooms where id = $1;`, [msg.roomID])).rows
        // console.log(roomData)

        await client.query(`
        insert into messages (room_id, project_id, sender_id, content, image) 
        values
        ($1, $2, $3, $4, $5);`, [msg.roomID, msg.pid, msg.uid, msg.content, msg.file])

        res.status(200).json(msg)
    } catch (error) {
        console.error('Failed to upload message to Server and Database.', error)
    }
}


spaceRoutes.post ('/chat/create/channel', async(req,res)=> {
    let pid = req.body.pid
    let name = req.body.name
    try {
        //check if the room exists
        let check = ((await client.query(`select * from rooms where project_id = $1 and name = $2;`, [pid, name]))).rows[0]

        if (!check) {
            //insert if no problem
            await client.query(`insert into rooms (project_id, name, accessibility) values ($1, $2, 'private');`, [pid, name])
            let newRoomID = ((await client.query(`select id from rooms where name = $1;`, [name]))).rows[0]
            res.status(200).json({message: 'Successfully Create Channel.', room:{pid, newRoomID, name}})
        } else {
            res.status(400).json({message: 'This channel already exits.'})
        }
    } catch (error) {
        console.log ('failed to create Chanel.', error)
        res.status(500).json({message: 'failed to create Chanel.'})
    }
})
