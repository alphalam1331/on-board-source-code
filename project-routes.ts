import express from 'express';
import { client } from './db'
import { loggedInOnly, captainOnly } from './guard';
import './session'
import multer from 'multer'

export const upload = multer({
    dest: './public/img/project'
})


export let projectRoutes = express.Router()

// Project Action ( join || abandon || follow || unfollow )
projectRoutes.get('/project/action/:pid/:action', loggedInOnly, async (req, res) => {
    const uid = req.session.user?.id
    const pid = +req.params.pid
    const action = req.params.action
    try {
        // delete old relation (if any)
        await client.query(`DELETE FROM join_project 
        WHERE user_id=$1 AND project_id=$2`, [uid, pid])

        // insert new relation ( follow || join )
        if (action == "follow") {
            await client.query(`INSERT INTO join_project 
            (user_id, project_id, role) 
            VALUES($1, $2, 'follower')`, [uid, pid])
            res.status(200).json({ message: "followed ship" })
        } else if (action == "join") {
            await client.query(`INSERT INTO join_project 
            (user_id, project_id, role) 
            VALUES($1, $2, 'pending')`, [uid, pid])
            res.status(200).json({ message: "pending approval" })

            // send res only ( unfollow || abandon )
        } else if (action == "unfollow") {
            res.status(200).json({ message: "unfollowed ship" })
        } else if (action == "abandon") {
            res.status(200).json({ message: "abandoned ship" })
        }
    } catch (err) {
        console.log(err)
        res.status(500).json({ message: "database error" })
    }
})

// Captain's Action ( approve )
projectRoutes.get('/project/approve/:pid/:uid', captainOnly, async (req, res) => {
    try {
        await client.query(`UPDATE join_project 
        SET role='crew'
        WHERE user_id=$1 AND project_id=$2`, [req.params.uid, req.params.pid])
        res.status(200).json({ message: "captain approved new member" })
    } catch (err) {
        console.log(err)
        res.status(500).json({ message: "database error" })
    }
})

projectRoutes.get('/project/reject/:pid/:uid', captainOnly, async (req, res) => {
    try {
        await client.query(`DELETE FROM join_project 
        WHERE user_id=$1 AND project_id=$2`, [req.params.uid, req.params.pid])
        res.status(200).json({ message: "captain rejected new member" })
    } catch (err) {
        console.log(err)
        res.status(500).json({ message: "database error" })
    }
})

// Get Project Information
projectRoutes.get('/project/search', async (req, res) => {
    try {
        // get all project id
        if (req.query.all) {
            let data = await client.query(`SELECT id FROM projects`)
            let allPid = data.rows.map(row => row.id)
            res.json(allPid)
            return
        }

        // keyword search
        if (req.query.keywords) {
            let list: string[] = []
            let keywords = (req.query.keywords as string).split(",")
            for (let keyword of keywords) {
                let keywordLower = "%" + keyword.toLowerCase() + "%"
                let result = await client.query(/* SQL */
                    `SELECT id FROM projects
                    WHERE LOWER (title) LIKE $1
                    OR LOWER (description) LIKE $1`,
                    [keywordLower]
                )

                let allResultId: string[] = result.rows.map(row => row.id)
                for (let item of allResultId) {
                    if (list.indexOf(item)) {
                        list.push(item)
                    }
                }
            }
            res.json(list)
            return
        }

        // filter search (by category or skills)
        let list = [] // arr to hold search results
        if (req.query.categories) { // add results by category
            // url format: ?categories=c1,c2,...
            let categories: string[] = (req.query.categories as string).split(",").map(str => str.toLowerCase())
            let sqlQuery = /* SQL */
                `SELECT DISTINCT projects.id 
                FROM projects 
                JOIN project_category 
                ON projects.id=project_category.project_id 
                JOIN categories 
                ON project_category.category_id=categories.id 
                WHERE LOWER (categories.name) IN (`

            for (let i = 1; i < categories.length; i++) {
                sqlQuery += `$${i},`
            }
            sqlQuery += `$${categories.length})`

            let result = await client.query(sqlQuery, categories)
            list = result.rows.map(row => row.id)
        }

        if (req.query.skills) { // add results by skill
            let skills: string[] = (req.query.skills as string).split(",").map(str => str.toLowerCase())
            let sqlQuery = /* SQL */
                `SELECT DISTINCT projects.id
                FROM projects 
                JOIN tasks
                ON projects.id=tasks.project_id
                JOIN required_skill
                ON tasks.id=required_skill.task_id
                JOIN skills
                ON required_skill.skill_id=skills.id
                WHERE LOWER (skills.name) IN (`

            for (let i = 1; i < skills.length; i++) {
                sqlQuery += `$${i},`
            }
            sqlQuery += `$${skills.length})`
            let result = await client.query(sqlQuery, skills)
            for (let id of result.rows.map(row => row.id)) {
                if (list.indexOf(id) < 0) {
                    list.push(id)
                }
            }
        }

        res.json(list) // send filtered list

    } catch (err) {
        console.log(err)
        res.status(500).json({ message: "Database Error" })
    }
})

projectRoutes.get('/project/load/:pid/', async (req, res, next) => {

    let pid = req.params.pid;
    let all = req.query.all;
    let images = req.query.images;
    let categories = req.query.categories;
    let skills = req.query.skills;
    let tasks = req.query.tasks;
    let rooms = req.query.rooms;
    //load primary project data including roles, crew members, required Skills
    try {
        let dbResult = ((await client.query(`select * from projects where id = $1;`, [pid]))).rows[0]

        dbResult.roles = ((await client.query( /* sql */
            `select users.id, users.username, users.propic, role, users.occupation  from projects
        inner join join_project 
            on projects.id = join_project.project_id 
        inner join users 
            on join_project.user_id = users.id where projects.id = $1
            order by
			case role
			when 'captain' then 1
			end;`, [pid]))).rows

        //load project_images
        if (images == "true" || all == 'true') {
            images = ((await client.query(`select filename from images where project_id = $1`, [pid]))).rows
            dbResult.images = []
            for (let image in images) {
                dbResult.images.push(images[image]['filename'])
            }
        }
        //load project_categories
        if (categories == "true" || all == 'true') {
            categories = ((await client.query(`select name from categories where id in (select category_id from project_category where project_id = $1);`, [pid]))).rows
            dbResult.categories = []
            for (let category in categories) {
                dbResult.categories.push(categories[category]['name'])
            }
        }
        //load project_skills
        if (skills == "true" || all == 'true') {
            skills = ((await client.query(`select distinct skills.name, skills.id from skills 
        inner join required_skill on skill_id = skills.id
        inner join tasks on tasks.id = task_id
        inner join projects on tasks.project_id = projects.id
        where projects.id = $1;`, [pid]))).rows
            dbResult.skills = []
            for (let skill in skills) {
                dbResult.skills.push(skills[skill]['name'])
            }
        }
        //load project_tasks and tasks members
        if (tasks == "true" || all == 'true') {
            dbResult.tasks = ((await client.query(`select tasks.title, users.propic, users.username from tasks
        inner join projects on projects.id = tasks.project_id
		left join join_task on tasks.id = join_task.task_id
        left join users on users.id = join_task.user_id
        left join join_project on join_project.id = join_task.join_project_id
        where projects.id =$1;`, [pid]))).rows
            for (let task of dbResult.tasks) {
                let results = ((await client.query(`select skills.name from skills
            inner join required_skill on skill_id = skills. id
            inner join tasks on task_id = tasks.id
            where tasks.title = $1;`, [task.title]))).rows
                let required_skill = []
                for (let result of results) {
                    required_skill.push(result.name)
                }
                task.required_skill = required_skill
            }
        }
        //load project_messages
        if (rooms == "true") {
            dbResult.rooms = ((await client.query(`select * from rooms where project_id = $1;`, [pid]))).rows
            // dbResult.messages = ((await client.query(`select * from messages where project_id = $1;`, [pid]))).rows
            // console.log ('found rooms: ',rooms)
        }
        // console.log (dbResult)
        res.json(dbResult)
    } catch (error) {
        console.log('Error in Loading project: ', error)
    }
})

projectRoutes.get('/project/categories', async (req, res) => {
    try {
        let result = await client.query( /* SQL */ 
            `SELECT name FROM categories
            WHERE id IN (
                SELECT DISTINCT category_id FROM project_category
            )`)
        res.json(result.rows.map(row => row.name))
    } catch (err) {
        console.log(err)
        res.status(500).json({ message: "database error" })
    }
})

projectRoutes.get('/project/skills', async (req, res) => {
    try {
        let result = await client.query( /* SQL */ 
            `SELECT name FROM skills
            WHERE id IN (
                SELECT DISTINCT skill_id FROM required_skill
            )`)
        res.json(result.rows.map(row => row.name))
    } catch (err) {
        console.log(err)
        res.status(500).json({ message: "database error" })
    }
})

type Member = {
    username: string,
    propic: string
}

projectRoutes.get('/project/load/:pid/members', async (req, res) => {
    let pid = req.params.pid
    let members: Member[] = []
    try {
        let result = ((await client.query(`
            select username, propic from users 
            inner join join_project on user_id = users.id
            inner join projects on join_project.project_id = projects.id
            where projects.id = $1;`, [pid]))).rows

        result.forEach(member => {
            members.push(member)
        })
        res.status(200).json(members)


    } catch (error) {
        console.log('failed to fetch members info: ', error)
        res.status(500).json({ message: 'failed to fetch members info' })

    }
})

projectRoutes.get('/project/load/:pid/:roomID/messages', async (req, res) => {
    let pid = req.params.pid
    let roomID = req.params.roomID
    try {
        let result = (await (client.query(`
        select users.username, messages.content, messages.image from messages
        inner join projects on projects.id = messages.project_id
        inner join rooms on rooms.id = messages.room_id
        inner join users on users.id = sender_id
        where projects.id = $1 and rooms.id = $2;`, [pid, roomID]
        ))).rows
        // console.log(result)
        res.status(200).json(result)
    } catch (error) {
        console.log('failed to load messages: ', error)
        res.status(500).json({ message: 'failed to load messages' })
    }

})

// Create | Edit Projects
projectRoutes.post('/project/create', loggedInOnly, upload.array('images', 3), async (req, res, next) => {
    try {
        let captainID = req.body.captainID
        // let captain = req.body.captain
        let title = req.body.title
        let description = req.body.description
        let start = req.body.start
        let end = req.body.end
        let images = req.files;
        // check if the title is duplicate
        let titleDuplicate = ((await client.query('select title from projects where title = $1;', [title]))).rowCount
        if (titleDuplicate != 0) {
            res.status(400).json({ message: `This project title "${title}" has been used.` })
            return
        }
        // insert into table: projects
        await client.query(`insert into projects (title, description, start_date, end_date) values ($1, $2, $3, $4);`, [title, description, start, end])
        //retrieve the newly inserted project's id
        const id = ((await client.query(`select id from projects where title = $1;`, [title]))).rows[0]['id'];
        //insert into table: join_project 
        await client.query(`insert into join_project (user_id, project_id, role) values ($1, $2, $3);`, [captainID, id, 'captain'])
        // insert images into project's related table: images
        for (let i = 0; i < images!.length; i++) {
            let filename = images![i].filename
            await client.query(`insert into images (filename, project_id)values ($1, $2);`, [filename, id])
        }
        await client.query(`insert into rooms (project_id, name, accessibility) VALUES ((select id from projects where title = $1 ) , 'Public', 'public');`, [title])
        res.status(200).json({ message: 'Successfully Created!' })
    } catch (error) {
        console.log('failed to insert data into projects table: ', error)
        res.status(400).json({ message: 'Please contact Admin: Carol Ma.' })
    }
})

projectRoutes.put('/project/edit', captainOnly, async (req, res) => {
    let pid = req.body.pid
    try {
        if (req.body.title) {
            await client.query(`update projects set title = $1 where id = $2;`, [req.body.title, pid])
        }
        if (req.body.description) {
            await client.query(`update projects set description = $1 where id = $2;`, [req.body.description, pid])
        }
        if (req.body.categories) {
            let categories = req.body.categories
            await client.query(`delete from project_category where project_id = $1;`, [pid])
            for (let category of categories) {
                let existingCate = ((await client.query(`select id from categories where name = $1;`, [category]))).rows[0]
                if (existingCate) {
                    console.log('this cate already exists.', { existingCate })
                    let cateID = existingCate.id
                    console.log(cateID)
                    await client.query(`insert into project_category (category_id, project_id) values ($1, $2);`, [cateID, pid])
                } else if (!existingCate) {
                    console.log('this is a new cate.', { category })
                    await client.query(`insert into categories (name) values ($1);`, [category]);
                    let newCateID = (await client.query(`select id from categories where name = $1;`, [category])).rows[0]['id']
                    console.log(newCateID)
                    await client.query(`insert into project_category (category_id, project_id) values ($1, $2);`, [newCateID, pid])
                }
            }
            res.status(200).json({ message: 'Successfully Update!', categories })
            return
        }
        if (req.body.tasks) {

            let tasks = req.body.tasks
            console.log ('tasks: ', tasks)
            await client.query(`insert into tasks (project_id, title, description, created, deadline)values ($1, $2, $3, $4, $5);`, [pid, tasks.title, tasks.description, tasks.startDate, tasks.endDate])
            let newTask = ((await client.query(`select id from tasks where title = $1;`, [tasks.title]))).rows[0]
            let taskID = newTask.id

            for (let skill of tasks.skill) {
                console.log ({skill})
                let existingSkill = ((await client.query(`select id from skills where name = $1;`, [skill]))).rows[0]

                if (existingSkill) {
                    let skillID = existingSkill.id
                    // console.log (skillID)
                    await client.query(`insert into required_skill (task_id, skill_id) values ($1, $2);`, [taskID, skillID])

                } else if (!existingSkill) {
                    await client.query(`insert into skills (name) values ($1);`, [skill]);
                    let newSkillID = ((await client.query(`select id from skills where name = $1;`, [skill]))).rows[0].id
                    await client.query(`insert into required_skill (task_id, skill_id) VALUES ($1,$2);`, [taskID, newSkillID])
                }
            }
            res.status(200).json({ message: 'Successfully Update!', tasks })
            return
        }
        res.status(200).json({ message: 'Successfully Update!' })
    } catch (error) {
        console.log(error)
        res.status(400).json({ message: 'Please contact Admin: Carol Ma.' })
    }
})



